'''
This script implements for testing my code only.
'''
import matplotlib.pyplot as plt
from utils import *

# emulate data
Results = dict()


bboxes =[
        ['image_1', 0.1, 0.1, 0.2, 0.2, 0.88, 0.3, 0, 1],
        ['image_1', 0.1, 0.1, 0.2, 0.2, 0.70, 0.9, 1, 0],
        ['image_1', 0.1, 0.1, 0.2, 0.2, 0.80, 0.3, 0, 1],
        ['image_2', 0.1, 0.1, 0.2, 0.2, 0.71, 0.3, 0, 1],
        ['image_2', 0.1, 0.1, 0.2, 0.2, 0.54, 0.9, 1, 0],
        ['image_2', 0.1, 0.1, 0.2, 0.2, 0.74, 0.3, 0, 1],
        ['image_3', 0.1, 0.1, 0.2, 0.2, 0.18, 0.9, 1, 0],
        ['image_3', 0.1, 0.1, 0.2, 0.2, 0.67, 0.3, 0, 1],
        ['image_3', 0.1, 0.1, 0.2, 0.2, 0.38, 0.3, 0, 1],
        ['image_3', 0.1, 0.1, 0.2, 0.2, 0.91, 0.9, 1, 0],
        ['image_3', 0.1, 0.1, 0.2, 0.2, 0.44, 0.3, 0, 1],
        ['image_4', 0.1, 0.1, 0.2, 0.2, 0.35, 0.3, 0, 1],
        ['image_4', 0.1, 0.1, 0.2, 0.2, 0.78, 0.3, 0, 1],
        ['image_4', 0.1, 0.1, 0.2, 0.2, 0.45, 0.3, 0, 1],
        ['image_4', 0.1, 0.1, 0.2, 0.2, 0.14, 0.3, 0, 1],
        ['image_5', 0.1, 0.1, 0.2, 0.2, 0.62, 0.9, 1, 0],
        ['image_5', 0.1, 0.1, 0.2, 0.2, 0.44, 0.3, 0, 1],
        ['image_5', 0.1, 0.1, 0.2, 0.2, 0.95, 0.9, 1, 0],
        ['image_5', 0.1, 0.1, 0.2, 0.2, 0.23, 0.3, 0, 1],
        ['image_6', 0.1, 0.1, 0.2, 0.2, 0.45, 0.3, 0, 1],
        ['image_6', 0.1, 0.1, 0.2, 0.2, 0.84, 0.3, 0, 1],
        ['image_6', 0.1, 0.1, 0.2, 0.2, 0.43, 0.3, 0, 1],
        ['image_7', 0.1, 0.1, 0.2, 0.2, 0.48, 0.9, 1, 0],
        ['image_7', 0.1, 0.1, 0.2, 0.2, 0.95, 0.3, 0, 1]
]

Results[0] = bboxes

Ground_truth = dict()
Ground_truth[0] = [1] * 15

Results_ordered, accu_tp, accu_fp, p, r = evaluate_precision_recall(Results, 0.31, Ground_truth)

# plt.plot(r[0], p[0])
# plt.show()

'''
def calc_average_precision(p, r, show_flag=False):
    """
    Calculate Average Precision by interpolating PR-curve.

    Note: Interpolation performed in all points.

    :param p: Precision points [list]
    :param r: Recall points    [list]
    :param show_flag: plot if TRUE  [boolean]

    :return: ap:        Average Precision
             p_interp:  interpolated precision
    """
    assert len(p) == len(r), "Equal number of Precision and Recall points."
    ap = 0
    # add starting point (r, p) = (0, 1)
    p = [1] + p
    r = [0] + r
    p_interp = [p[0]]

    for i in range(len(p)-1):
        interp = max(p[i:])
        ap += (r[i+1] - r[i]) * interp
        p_interp.append(interp)

    if show_flag:
        plt.plot(r, p)
        plt.step(r, p_interp)

        plt.legend(['Precision', 'Interpolated precision'], loc='upper right')
        plt.show()

    return ap, p_interp
'''

ap, _ = calc_average_precision(p[0], r[0], True)
print('AP = {}'.format(ap))
print('done.')