'''
This script implements the training procedure.
'''
from train import *

if __name__ == "__main__":

    # Dataset
    voc2012 = VOC('/Users/erica/Dataset/Pascal/2012_train_short.txt', IMG_WIDTH, IMG_HEIGHT)
    det = ground_truth_detection(voc2012.label_list)

    # Feed model
    yolo_model = build_darknet()

    # Random Data
    X = torch.randn(20, 3, 448, 448)                 # image batch (random)
    Y = torch.clamp(torch.randn(20, 7, 7, 5), 0, 1)  # label batch (random)

    # Prediction
    Y_out = yolo_model(X)
    # Y_pred = [predict_one_bbox(Y_out[i].clone(), Y[i].clone()) for i in range(20)]
    # Y_pred = torch.stack(Y_pred)

    # Loss
    total_loss = calc_loss(Y_out, Y)
    print('total loss = ', total_loss)

    # Optimizer
    learning_rate = 1e-4
    optimizer = torch.optim.Adam(yolo_model.parameters(), lr=learning_rate)

    # Training
    for t in range(3):
        # forward pass
        Y_out = yolo_model(X)

        # compute loss
        loss = calc_loss(Y_out.clone(), Y.clone())

        print('\nEpoch = ', t, 'Loss = ', loss.item())
        print('\n---------------------------------------')

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

    print('Done.')
